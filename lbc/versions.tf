# Terraform Settings Block
terraform {
  required_version = "1.6.6"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.19.0"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.12.1"
    }
    http = {
      source = "hashicorp/http"
      #version = "2.1.0"
      version = "2.2.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.11.0"
    }
  }
  # Adding Backend as S3 for Remote State Storage
  backend "s3" {
    bucket = "terraform-aws-eks-ap-assignment"
    key    = "dev/aws-lbc/terraform.tfstate"
    region = "ap-southeast-1"

    # For State Locking
    dynamodb_table = "dev-aws-lbc"
  }
}

# Terraform AWS Provider Block
provider "aws" {
  region = var.aws_region
}

# Terraform HTTP Provider Block
provider "http" {
  # Configuration options
}