# Resource: Kubernetes Namespace yuzee-dev
resource "kubernetes_namespace_v1" "yuzee-dev" {
  metadata {
    name = "littlelives"
  }
}

resource "kubernetes_service_account" "yuzee-dev" {
  metadata {
    name      = "littlelives"
    namespace = "littlelives"

  }
}