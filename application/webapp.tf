resource "kubernetes_deployment" "web_app" {
  metadata {
    name = "web-app"
    namespace = "littlelives"
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "web-app"
      }
    }

    template {
      metadata {
        labels = {
          app = "web-app"
        }
      }

      spec {
        container {
          name  = "web-app"
          image = "mossesmos/littlelives-1:latest"

          port {
            container_port = 3000
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "web_app_service" {
  metadata {
    name = "web-app-service"
    namespace = "littlelives"
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 80
      target_port = "3000"
    }

    selector = {
      app = "web-app"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_ingress_v1" "web_app_ingress" {
  metadata {
    name = "web-app-ingress"
    namespace = "littlelives"
  }

  spec {
    rule {
      host = "app.example.com"

      http {
        path {
          path      = "/"
          path_type = "Prefix"

          backend {
            service {
              name = "web-app-service"

              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}

