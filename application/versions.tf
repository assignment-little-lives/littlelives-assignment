# Terraform Settings Block
terraform {
  required_version = ">= 1.6.6"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.19.0"
    }

    helm = {
      source = "hashicorp/helm"
      #version = "2.5.1"
      version = "~> 2.12.1"
    }
  }
  # Adding Backend as S3 for Remote State Storage
  backend "s3" {
    bucket = "terraform-aws-eks-ap-assignment"
    key    = "dev/eks-yuzee-app/terraform.tfstate"
    region = "ap-southeast-1"

    # For State Locking
    dynamodb_table = "dev-eks-yuzee-app"
  }
}

# Terraform AWS Provider Block
provider "aws" {
  region = var.aws_region
}

