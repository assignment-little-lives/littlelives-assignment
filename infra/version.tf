terraform {
  required_version = "1.6.6"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.19.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.11.0"
    }
  }
  backend "s3" {
    bucket = "terraform-aws-eks-ap-assignment"
    key    = "dev/eks-cluster/terraform.tfstate"
    region = "ap-southeast-1"

    dynamodb_table = "dev-ekscluster"

  }
}
provider "aws" {
  region = var.aws_region

}
