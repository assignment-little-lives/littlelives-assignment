

resource "aws_eks_node_group" "eks_ng_private" {
  cluster_name = aws_eks_cluster.eks_cluster.name

  node_group_name = "${local.name}-eks-ng-private"
  node_role_arn   = aws_iam_role.eks_nodegroup_role.arn
  subnet_ids      = module.vpc.private_subnets
  version         = var.cluster_version #optional

  ami_type       = "AL2_x86_64"
  capacity_type  = "SPOT"
  disk_size      = 20
  instance_types = ["t3a.large"]

  remote_access {
    ec2_ssh_key = "eks-terraform-key"
  }

  scaling_config {
    desired_size = 1
    max_size     = 10
    min_size     = 1

  }

  update_config {
    max_unavailable = 1
    #max_unavailable_percentage = 50
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.eks-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.eks-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.eks-AmazonEC2ContainerRegistryReadOnly,
    kubernetes_config_map_v1.aws_auth
  ]
  tags = {
    Name = "Private-Node-Group"
    # Cluster Autoscaler Tags
    "k8s.io/cluster-autoscaler/${local.eks_cluster_name}" = "owned"
    "k8s.io/cluster-autoscaler/enabled"                   = "TRUE"
    "karpenter.sh/discovery"                              = "${local.eks_cluster_name}"

  }
}


resource "aws_eks_node_group" "eks_ng_private-1" {
  depends_on = [aws_eks_node_group.eks_ng_private]
  cluster_name = aws_eks_cluster.eks_cluster.name

  node_group_name = "${local.name}-eks-ng-private-1"
  node_role_arn   = aws_iam_role.eks_nodegroup_role.arn
  subnet_ids      = module.vpc.private_subnets
  version         = var.cluster_version #optional

  ami_type       = "AL2_x86_64"
  capacity_type  = "ON_DEMAND"
  disk_size      = 20
  instance_types = ["t3a.large"]

  remote_access {
    ec2_ssh_key = "eks-terraform-key"
  }

  scaling_config {
    desired_size = 1
    max_size     = 10
    min_size     = 1

  }

  update_config {
    max_unavailable = 1
    #max_unavailable_percentage = 50
  }

  tags = {
    Name = "Private-Node-Group-1"
    # Cluster Autoscaler Tags
    "k8s.io/cluster-autoscaler/${local.eks_cluster_name}" = "owned"
    "k8s.io/cluster-autoscaler/enabled"                   = "TRUE"
    "karpenter.sh/discovery"                              = "${local.eks_cluster_name}"

  }
}


resource "aws_autoscaling_group_tag" "eks_ng_private" {    
  autoscaling_group_name = aws_eks_node_group.eks_ng_private.resources[0].autoscaling_groups[0].name

  tag {
    key   = "Name"
    value = "eks-dev-spot"

    propagate_at_launch = true
  }

  depends_on = [
    aws_eks_node_group.eks_ng_private
  ]
}

resource "aws_autoscaling_group_tag" "eks_ng_private-1" {    
  autoscaling_group_name = aws_eks_node_group.eks_ng_private-1.resources[0].autoscaling_groups[0].name

  tag {
    key   = "Name"
    value = "eks-dev-on-demand"

    propagate_at_launch = true
  }

  depends_on = [
    aws_eks_node_group.eks_ng_private-1
  ]
}


