Prerequsites before running the terraform script 
- Adding the DynamoDB Tables for Modules to lock the state
- Creating a new s3 bucket and adding it to version.tf 
- For cicd adding the AWS access and secret keys
- adding the keypar "eks-terraform-key"

1. Running the infra module would provison 3 tier vpc with EKS cluster and appropriate
nodepools. 
2. Running lbc module will provison alb load balancer
3. Running the application module will provision the deployment, service and ingress 
to littlelives namespace in EKS.

