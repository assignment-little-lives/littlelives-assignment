# Use an official Node base image
FROM node:21.4.0

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json for npm install
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the local code to the container
COPY . .


COPY packages/server/.env-example packages/server/.env

# Build the application
RUN npm run build

# Start the application
CMD ["npm", "start"]

